import express from 'express';

const port = 3000;
const app = express();

const handleRequest = (request, response) => {
    response.json({"message": "hello world!"});
};

const handleApp = () => {
    console.log(`start server at port ${port}`);
}

app.get('/', handleRequest);
app.listen(port, handleApp);
